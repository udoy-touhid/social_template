package com.apps.hsc_result_web;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.plus.PlusShare;

public class share_activity extends AppCompatActivity {
    final String PACKAGENAME = "com.apps.hsc_result_web";
    final String APP_TITLE="সিমের টুকিটাকি";
    final String APP_DESC="App having large collection of mobile utility";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share);

        Button fb = (Button) findViewById(R.id.facebook);
        fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CallbackManager callbackManager;
                ShareDialog shareDialog;
                FacebookSdk.sdkInitialize(getApplicationContext());
                callbackManager = CallbackManager.Factory.create();
                shareDialog = new ShareDialog(share_activity.this);
                if (ShareDialog.canShow(ShareLinkContent.class)) {
                    ShareLinkContent linkContent = new ShareLinkContent.Builder()
                            .setContentTitle(APP_TITLE)
                            .setContentDescription(APP_DESC)
                            .setContentUrl(Uri.parse("http://play.google.com/store/apps/details?id=" + PACKAGENAME))
                            .build();
                    shareDialog.show(linkContent);
                }

            }
        } );
        Button gplus = (Button) findViewById(R.id.gplus);
        gplus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent shareIntent = new PlusShare.Builder(share_activity.this)
                        .setType("text/plain")
                        .setContentUrl(Uri.parse("https://play.google.com/store/apps/details?id=" + PACKAGENAME))
                        .getIntent();

                startActivityForResult(shareIntent, 0);
            }
        });


    }

}
